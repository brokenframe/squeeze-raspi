import logging
import paho.mqtt.client as mqtt
import Adafruit_CharLCD as LCD
from RPi import GPIO
import json
from threading import Timer
from gen.IDisplayContent import IDisplayContentReceiver

LCD_RS = 7
LCD_E  = 8
LCD_DATA4 = 25
LCD_DATA5 = 24
LCD_DATA6 = 23
LCD_DATA7 = 18
LCD_COLS = 16
LCD_ROWS = 2

class PresentationController (IDisplayContentReceiver):

    # constructor
    def __init__(self):
        self.client = mqtt.Client("PresentationController")
        self.client.on_log=self.on_log
        self.__currentTrack = ''
        self._currentLine1 = ''
        self._currentLine2 = ''

    def onCurrentTrack(self, title, artist):
        self.display(artist, title)
        self.__currentTrack = '{"artist":"'+artist+'", "title":"'+title+'"}'
    	
    def onCurrentVolume(self, volume):
        self.display("Volume", str(volume))
        t = Timer(5, self.updateWithTrack)
        t.start()
    	
    def onPlayingStatus(self, status):
        if(status == 'Paused'):
            self.display("Status", status)
        else:
            self.updateWithTrack()
    	
    def onMutingStatus(self, status):
        if(status == 'Muted'):
            self.display("Status", status)
        else:
            self.updateWithTrack()
   	
    def onGeneralMessage(self, line1, line2):
        self.display(line1, line2)

    def on_log(self, client, userdata, level, buf):
        logging.debug("PresentationController %s" % buf)

    def start(self):
        self.client.connect('127.0.0.1')
        self.client.loop_start()
        self.registerForCallbacks(self.client, self)
        # init display
        GPIO.setwarnings(False)
        self.lcd = LCD.Adafruit_CharLCD(LCD_RS, LCD_E, LCD_DATA4, LCD_DATA5, LCD_DATA6, LCD_DATA7, LCD_COLS, LCD_ROWS)
        self.lcd.clear()
	
    
    def stop(self):
        GPIO.cleanup()
        self.client.loop_stop()

    def display(self, str1, str2):
        logging.debug("PresentationController: Send to Display %s %s", str1, str2)
        if (str1 != self._currentLine1 or str2 != self._currentLine2):
            self.lcd.clear()
            self.lcd.message(str1+'\n'+str2)
            self._currentLine1 = str1
            self._currentLine2 = str2
        else:
            logging.debug("PresentationController: Display content unchanged. Do not update") 


    def updateWithTrack(self):
        track = json.loads(self.__currentTrack)
        logging.debug("updateWithTrack %s", track)
        self.display(track["artist"], track["title"])
