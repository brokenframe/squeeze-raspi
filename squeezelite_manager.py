import logging
import json
import time
import socket
import paho.mqtt.client as mqtt
from pylms.server import Server
from pylms.player import Player

from gen.ISqueezePlayer import ISqueezePlayerPublisher
from gen.IDisplayContent import IDisplayContentPublisher

class SqueezeliteManager ():


    #todo get class name automatically
    def __init__(self, name="SqueezeliteManager"):
        self.client = mqtt.Client(name)
        self.client.on_log=self.on_log
        self.__currentTitle = ''
        self.__currentArtist = ''
        self.__currentVolume = 0

    def on_log(self, client, userdata, level, buf):
        logging.debug("SqueezeliteManager %s", buf)

    def start(self):
        logging.debug("Loading credentials")
        config = json.loads(open('connection.json').read())
        chost = config["hostname"]
        cport = config["port"]
        cuser = config["username"]
        cpw = config["pw"]
        logging.debug(config)
    
        sc = Server(hostname=chost, port=cport, username=cuser, password=cpw)
        not_connected = True
        while not_connected:
            try:
                sc.connect()
                not_connected = False
            except socket.error:
                logging.warning('Cannot not connect to squeezelite server. Try again in 2 seconds...')
                time.sleep(2)

        logging.info ("Successful connected to sqeezelite server")
        logging.info ("Logged in: %s" % sc.logged_in )
        logging.info ("Version : %s" % sc.get_version() )

        self.player = sc.get_players()[0]

        self.client.connect("127.0.0.1")

        self.__currentVolume = self.player.get_volume()

        self.displayContentPublisher = IDisplayContentPublisher(self.client)
        self.squeezePublisher = ISqueezePlayerPublisher(self.client)
        
        self.client.loop_start()        

    def stop(self):
        self.client.loop_stop()

    def checkForChanges(self):
        if(self.__currentTitle != self.player.get_track_title() or self.__currentArtist != self.player.get_track_artist()):
            self.__currentTitle = self.player.get_track_title()
            self.__currentArtist = self.player.get_track_artist()
            self.squeezePublisher.fireCurrentTrack(self.__currentTitle, self.__currentArtist)
        if(self.__currentVolume != self.player.get_volume()):
            self.__currentVolume = self.player.get_volume()
            self.squeezePublisher.fireCurrentVolume(self.__currentVolume)
            
    def nextTrack(self):
        self.player.next()

    def previousTrack(self):
        self.player.prev()

    def volumeUp(self):
        self.player.volume_up()

    def volumeDown(self):
        self.player.volume_down()

    def getVolume(self):
        return self.player.get_volume()

    def toggleTrack(self):
        self.player.toggle()
        logging.debug("Mode %s", self.player.get_mode())
        if(self.player.get_mode() == 'pause'):
            self.displayContentPublisher.firePlayingStatus('Paused')
        else:
            self.displayContentPublisher.firePlayingStatus('Play')
            
    def toggleMute(self):
        if self.player.get_volume() < 0:
            self.player.unmute()
            self.displayContentPublisher.fireMutingStatus('Unmuted')
        else:
            self.player.mute()
            self.displayContentPublisher.fireMutingStatus('Muted')
        
        
