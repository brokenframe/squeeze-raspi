#!/bin/bash
# Stop the app
kill $(ps aux | grep '[p]ython radio_app.py' | awk '{print $2}')
# Stop solys-agent
kill $(ps aux | grep '[s]olys-agent' | awk '{print $2}')
