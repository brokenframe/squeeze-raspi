import json
import logging

BUTTONPRESSED_EVENT = 'ui/ctrl/rotarycontroller/buttonPressed'
ROTATED_EVENT = 'ui/ctrl/rotarycontroller/rotated'
ALL_EVENTS = 'ui/ctrl/rotarycontroller/#'

class IRotaryControllerPublisher ():

    def __init__(self, client):
        self.__client = client
    
    def fireButtonPressed(self, instanceId):
    	payload = {}
    	payload['instanceId'] = instanceId
    	logging.debug('%s.fireButtonPressed', __name__)
    	logging.debug('%s %s', BUTTONPRESSED_EVENT, json.dumps(payload))
    	self.__client.publish(BUTTONPRESSED_EVENT, json.dumps(payload))
    	
    def fireRotated(self, instanceId, direction):
    	payload = {}
    	payload['instanceId'] = instanceId
    	payload['direction'] = direction
    	logging.debug('%s.fireRotated', __name__)
    	logging.debug('%s %s', ROTATED_EVENT, json.dumps(payload))
    	self.__client.publish(ROTATED_EVENT, json.dumps(payload))
    	

class IRotaryControllerReceiver ():

    def registerForCallbacks(self, client, callback):
        client.subscribe(ALL_EVENTS)
        client.message_callback_add(ALL_EVENTS, self.__dispatch)
        self.__callback = callback
    
    def __dispatch(self, client, userdata, message):
    	value = str(message.payload.decode('utf-8'))
    	t = message.topic
    	if t == BUTTONPRESSED_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onButtonPressed', __name__)
    		self.__callback.onButtonPressed(data['instanceId'])
    	if t == ROTATED_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onRotated', __name__)
    		self.__callback.onRotated(data['instanceId'], data['direction'])
    		


class IRotaryControllerCallback ():
	
	def onButtonPressed(self, instanceId):
		raise NotImplementedError("Please Implement this method")
		
	def onRotated(self, instanceId, direction):
		raise NotImplementedError("Please Implement this method")
		

