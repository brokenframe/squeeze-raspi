import json
import logging

CURRENTTRACK_EVENT = 'remotecontrol/squeezeplayer/currentTrack'
CURRENTVOLUME_EVENT = 'remotecontrol/squeezeplayer/currentVolume'
ALL_EVENTS = 'remotecontrol/squeezeplayer/#'

class ISqueezePlayerPublisher ():

    def __init__(self, client):
        self.__client = client
    
    def fireCurrentTrack(self, title, artist):
    	payload = {}
    	payload['title'] = title
    	payload['artist'] = artist
    	logging.debug('%s.fireCurrentTrack', __name__)
    	logging.debug('%s %s', CURRENTTRACK_EVENT, json.dumps(payload))
    	self.__client.publish(CURRENTTRACK_EVENT, json.dumps(payload))
    	
    def fireCurrentVolume(self, volume):
    	payload = {}
    	payload['volume'] = volume
    	logging.debug('%s.fireCurrentVolume', __name__)
    	logging.debug('%s %s', CURRENTVOLUME_EVENT, json.dumps(payload))
    	self.__client.publish(CURRENTVOLUME_EVENT, json.dumps(payload))
    	

class ISqueezePlayerReceiver ():

    def registerForCallbacks(self, client, callback):
        client.subscribe(ALL_EVENTS)
        client.message_callback_add(ALL_EVENTS, self.__dispatch)
        self.__callback = callback
    
    def __dispatch(self, client, userdata, message):
    	value = str(message.payload.decode('utf-8'))
    	t = message.topic
    	if t == CURRENTTRACK_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onCurrentTrack', __name__)
    		self.__callback.onCurrentTrack(data['title'], data['artist'])
    	if t == CURRENTVOLUME_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onCurrentVolume', __name__)
    		self.__callback.onCurrentVolume(data['volume'])
    		


class ISqueezePlayerCallback ():
	
	def onCurrentTrack(self, title, artist):
		raise NotImplementedError("Please Implement this method")
		
	def onCurrentVolume(self, volume):
		raise NotImplementedError("Please Implement this method")
		

