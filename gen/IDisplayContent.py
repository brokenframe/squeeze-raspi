import json
import logging

CURRENTTRACK_EVENT = 'ui/display/displaycontent/currentTrack'
CURRENTVOLUME_EVENT = 'ui/display/displaycontent/currentVolume'
PLAYINGSTATUS_EVENT = 'ui/display/displaycontent/playingStatus'
MUTINGSTATUS_EVENT = 'ui/display/displaycontent/mutingStatus'
GENERALMESSAGE_EVENT = 'ui/display/displaycontent/generalMessage'
ALL_EVENTS = 'ui/display/displaycontent/#'

class IDisplayContentPublisher ():

    def __init__(self, client):
        self.__client = client
    
    def fireCurrentTrack(self, title, artist):
    	payload = {}
    	payload['title'] = title
    	payload['artist'] = artist
    	logging.debug('%s.fireCurrentTrack', __name__)
    	logging.debug('%s %s', CURRENTTRACK_EVENT, json.dumps(payload))
    	self.__client.publish(CURRENTTRACK_EVENT, json.dumps(payload))
    	
    def fireCurrentVolume(self, volume):
    	payload = {}
    	payload['volume'] = volume
    	logging.debug('%s.fireCurrentVolume', __name__)
    	logging.debug('%s %s', CURRENTVOLUME_EVENT, json.dumps(payload))
    	self.__client.publish(CURRENTVOLUME_EVENT, json.dumps(payload))
    	
    def firePlayingStatus(self, status):
    	payload = {}
    	payload['status'] = status
    	logging.debug('%s.firePlayingStatus', __name__)
    	logging.debug('%s %s', PLAYINGSTATUS_EVENT, json.dumps(payload))
    	self.__client.publish(PLAYINGSTATUS_EVENT, json.dumps(payload))
    	
    def fireMutingStatus(self, status):
    	payload = {}
    	payload['status'] = status
    	logging.debug('%s.fireMutingStatus', __name__)
    	logging.debug('%s %s', MUTINGSTATUS_EVENT, json.dumps(payload))
    	self.__client.publish(MUTINGSTATUS_EVENT, json.dumps(payload))
    	
    def fireGeneralMessage(self, line1, line2):
    	payload = {}
    	payload['line1'] = line1
    	payload['line2'] = line2
    	logging.debug('%s.fireGeneralMessage', __name__)
    	logging.debug('%s %s', GENERALMESSAGE_EVENT, json.dumps(payload))
    	self.__client.publish(GENERALMESSAGE_EVENT, json.dumps(payload))
    	

class IDisplayContentReceiver ():

    def registerForCallbacks(self, client, callback):
        client.subscribe(ALL_EVENTS)
        client.message_callback_add(ALL_EVENTS, self.__dispatch)
        self.__callback = callback
    
    def __dispatch(self, client, userdata, message):
    	value = str(message.payload.decode('utf-8'))
    	t = message.topic
    	if t == CURRENTTRACK_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onCurrentTrack', __name__)
    		self.__callback.onCurrentTrack(data['title'], data['artist'])
    	if t == CURRENTVOLUME_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onCurrentVolume', __name__)
    		self.__callback.onCurrentVolume(data['volume'])
    	if t == PLAYINGSTATUS_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onPlayingStatus', __name__)
    		self.__callback.onPlayingStatus(data['status'])
    	if t == MUTINGSTATUS_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onMutingStatus', __name__)
    		self.__callback.onMutingStatus(data['status'])
    	if t == GENERALMESSAGE_EVENT:
    		data = json.loads(value)
    		logging.debug('%s.onGeneralMessage', __name__)
    		self.__callback.onGeneralMessage(data['line1'], data['line2'])
    		


class IDisplayContentCallback ():
	
	def onCurrentTrack(self, title, artist):
		raise NotImplementedError("Please Implement this method")
		
	def onCurrentVolume(self, volume):
		raise NotImplementedError("Please Implement this method")
		
	def onPlayingStatus(self, status):
		raise NotImplementedError("Please Implement this method")
		
	def onMutingStatus(self, status):
		raise NotImplementedError("Please Implement this method")
		
	def onGeneralMessage(self, line1, line2):
		raise NotImplementedError("Please Implement this method")
		

