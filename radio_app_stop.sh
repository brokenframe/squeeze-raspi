#!/bin/bash
# Stop the app
kill $(ps aux | grep '[p]ython radio_app.py' | awk '{print $2}')
