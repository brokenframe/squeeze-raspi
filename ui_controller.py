import logging
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
from thirdparty.KY040 import KY040
from gen.IRotaryController import IRotaryControllerPublisher

class UIController ():
    def __init__(self, name, clockpin, datapin, switchpin):
        self.client = mqtt.Client(name)
        self.client.on_log=self.on_log
        self.name = name
        self.clockpin = clockpin
        self.datapin = datapin
        self.switchpin = switchpin

        self.publisher = IRotaryControllerPublisher(self.client)

    def on_log(self, client, userdata, level, buf):
        logging.debug("UIController %s %s", self.name, buf)

    def start(self):
        GPIO.setmode(GPIO.BCM)
        sensor = KY040(self.clockpin, self.datapin, self.switchpin, self.rotateCallback, self.buttonPressedCallback)
        sensor.start()
        self.client.connect("127.0.0.1")
        self.client.loop_start()        

    def rotateCallback(self, direction):
        if(direction == 1):
            self.publisher.fireRotated(self.name, 'down')
        else:
            self.publisher.fireRotated(self.name, 'up')


    def buttonPressedCallback(self, pin):
        self.publisher.fireButtonPressed(self.name)


    def stop(self):
        self.client.loop_stop()
        
