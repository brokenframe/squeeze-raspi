# Overview

This project is about building a home-made multiroom media player solution, embedded into the frame of an old tube radio.
In my case I have used the body of an old AEG radio.

![alt text](images/aeg_radio.JPG)

The application is implemented in Python and running under Linux on a Raspberry Pi 3 Model B. 
It is build upon the Logitech Media Server architecture, which in principle requires three modules:

* A server streaming audio files from your mp3 libraries and internet radio stations
* A client (player) running on the device and playing back the audio on connected loudspeakers
* An app running on a smartphone for remote control the server and the player

For more information read here: 
* https://en.wikipedia.org/wiki/Logitech_Media_Server
* https://www.mysqueezebox.com/index/Home

As a special gadget I am re-using the radio's origin rotary knobs to control the player and show current track information on an integrated LCD display.

Thus, the entire eco-system looks like this:

![alt text](images/squeezebox_setup.png)

* Raspberry PI 3 Model B (including a breadboard and jumper cables for connecting the display and the rotary controllers)
* USB Soundcard
* USB Loudspeaker
* 2 Rotary Controllers
* 1 LCD Display
* A Logic Media Server compliant player (in my case squeezelite)
* A Squeezebox remote control running on a smartphone
* Logitech Media Server, aka  SlimServer, SqueezeCenter or Squeezebox Server (in my case running on a WD My Cloud)
* A body to install the components (in my case an old AEG radio frame)

# Hardware

## Raspberry PI 3 Model B

The rotary controllers and the LCD display are connected as illustrated in following wiring diagram:

![alt text](images/squeeze_radio_platine.png)

## Rotary Controllers

The radio is controlled with two rotary controllers, one for controlling the volume, the other for controlling the track/radio station handling.
I am using the KY040 rotary controllers:

https://www.amazon.de/PowerArt-bi00172-KY-040-Drehwinkelgeber-Modul-Arduino/dp/B00HSX9ZB2

Be aware, that the controller is unfortunately not shipped with a corresponding nut.
It took me a while to find out, that I need a "M7 x 0.75" nut.

## LCD Display

The display is used to show information about the current track (title and artist) or status information, such as current volume or player status.
I am using a display with a HD44780 controller.

https://www.amazon.de/Zeichen-Display-HD44780-Backlight-Arduino-Blau/dp/B009GEPZRE

## USB Soundcard

The Raspberry PI is in fact shipped with a built-in audio chip, but it is of mediocre quality.
So, I suggest to install an extra USB soundcard instead, like the following:
https://www.amazon.de/UGREEN-Soundkarte-Kopfh%C3%B6rer-Mikrofonanschluss-kompatibel/dp/B016CU2PEU

## USB Loudspeaker

I am using loudspeakers which don't need an additional power supply, but are served via the USB port from the Raspberry PI, like the following:
https://www.logitech.com/de-de/product/stereo-speakers-z120


# Software

The radio can be controlled in parallel from two different input devices:
* The built-in rotary knobs
* The smartphone app

This repository contains the source code to collect the players data, such as the currently played track or radio station and show the information on the intergrated LCD display.
It also reacts on events from the origin rotary knobs and triggers corresponding actions to the player, like chaning the volume or skipping to next or previous tracks.

The software is tested and running under raspbian OS version 4.9.

Before starting installing any software it is alwasy a good idea you to update the system first.

```sudo apt-get update```

```sudo apt-get upgrade```

## Install squeezelite player

First you need to install squeezelite, the software which is playing back the audio stream on your Raspberry PI device.

```sudo apt-get install squeezelite```

## Install Mosquitto (MQTT broker)

Next is installing the MQTT broker mosquitto. MQTT is a publish/subscribe framework known from the IoT world. I am using it to send messages in-between software modules (e.g. between the player and the display). 

```sudo apt-get install mosquitto```

## Application

Then get the source code of this application by cloning the repository to a directory of your choice:

```git clone https://gitlab.com/brokenframe/squeeze-raspi.git```

* radio_app.py - the main application
* ui_controller.py - notifies on controller actions, like rotating or pressing the rotary controller
* squeezelite_manager.py - bridge to the squeezelite player
* presentation_controller.py - manages the content to be displayed on the LCD display
* gen - folder that contains generated python code for dealing with MQTT messages
* thirdparty - folder to locate third party libraries (see details below)
* service - folder that contains scripts for autostart the application after booting (see details below)

## Used thirdparty libraries

### Install KY040 (rotary controller) driver

Download https://github.com/martinohanlon/KY040/blob/master/ky040/KY040.py and copy it to the folder _thirdparty_

### Install LCD driver

I am using a library from Adafruit to control the LCD. Install it with:

```sudo pip install adafruit-charlcd```

### Install Logitech Media Server Python API

This library allows accessing the Logitech Media Server and its connected player(s). Install it with:

```sudo easy_install pylms```

More information can be found here: https://github.com/jinglemansweep/PyLMS

### Install Eclipse Paho MQTT Python API

The MQTT messaging broker can be installed with following command

```sudo pip install paho-mqtt```

More information can be found here: https://pypi.python.org/pypi/paho-mqtt#installation

## Autostart

This step is needed if you want the application stack to launch and connect to the server automatically after starting up the device. Linux offers different concepts to auto start applications. I decided to use the systemd infrastructure. To make the application systemd-aware you need to provide so-called service files, which are located under the folder ./service.

The squeeze.service contains the reference to your media player. Means you need to change the MAC address accordingly to the one from your Raspberry PI.
You can alos change the name. I called my player AEG Radio, because it is embedded into an old AEG radio frame.

```ExecStart=/usr/bin/squeezelite -m [xx:xx:xx:xx:xx:xx] -n "AEG Radio"```

Then copy the service files to the systemd directory on your device

```sudo cp ./service/*.service /lib/systemd/system```

Then enable the services

```sudo systemctl enable squeeze.service```

```sudo systemctl enable radioapp.service```

After rebooting the system, your application should start-up automatically.

## Install Logitech Media Server

You need to install a Logitech Media Server somewhere in your network. A list of servers can be found here https://www.mysqueezebox.com/download.
I am running it as an App within a WD My Cloud.

## Install Squeezelite App (e.g. iPhone)

There are several apps on iOS and Android working as squeezebox controllers. 
I am using that following app: https://itunes.apple.com/de/app/logitech-squeezebox-controller/id431302899?mt=8

## Audio Settings

You need to tell your Raspberry PI to use the USB soundcard instead the built-in audiochip. 
There are different options. I am using the GUI for it.
Go to Settings -> Audi Device Settings and select USB Audio Device as soundcard

## Configuration

The project contains a file named _connection_template.json_. It contains the IP address of your Logitech Media Server and your login credentials.
Rename it to _connection.json_ and adapt it to your configuration settings.


Puh... That finally is it.
Power on the radio and control it via app or the built-in controllers...
