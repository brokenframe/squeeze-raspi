# -*- encoding: utf-8 -*-
#!/usr/bin/env python

import time
import sys
import logging
import socket
import paho.mqtt.client as mqtt
from subprocess import call

from ui_controller import UIController
from presentation_controller import PresentationController
from squeezelite_manager import SqueezeliteManager

from gen.IRotaryController import IRotaryControllerReceiver, IRotaryControllerCallback
from gen.ISqueezePlayer import ISqueezePlayerReceiver, ISqueezePlayerCallback
from gen.IDisplayContent import IDisplayContentPublisher

############ VOLUME CONTROLLER ############
# GPIO pins for the volume controller
# rotate means volume up and down
# press the button means mute / unmute
CLOCKPIN_VLM_CTRL = 5
DATAPIN_VLM_CTRL = 6
SWITCHPIN_VLM_CTRL = 13

############ TRACK CONTROLLER #############
# GPIO pins for the track controller
# rotate means forward and backward
# press the button means play / pause
CLOCKPIN_TRK_CTRL = 17
DATAPIN_TRK_CTRL = 27
SWITCHPIN_TRK_CTRL = 22

class SqueezePlayerCallback (ISqueezePlayerCallback):
    
    def onCurrentTrack(self, title, artist):
        displayContent.fireCurrentTrack(title, artist)
    	
    def onCurrentVolume(self, volume):
        displayContent.fireCurrentVolume(volume)

class RotaryControllerCallback(IRotaryControllerCallback):


    def __shuttingDown(self):
        displayContent.fireGeneralMessage('Shutting Down', 'Good Bye')
        time.sleep(2)
        logging.info("Shutting down")
        call("sudo shutdown -h +1", shell=True)
        call("sudo systemctl stop radioapp.service", shell=True)


    def onButtonPressed(self, instanceId):
        logging.info("RotaryControllerReceiver::onButtonPressed")

        if(instanceId == 'volumeCtrl'):
            squeeze.toggleMute()
        elif (instanceId == 'trackCtrl'):
            #squeeze.toggleTrack()
            self.__shuttingDown()


    def onRotated(self, instanceId, direction):
        logging.info("RotaryControllerReceiver::onRotated")

        if(instanceId == 'volumeCtrl'):
            if direction == 'up':
                squeeze.volumeUp()
            else:
                squeeze.volumeDown()
            logging.info('Volume %d' % squeeze.getVolume())
            displayContent.fireCurrentVolume(squeeze.getVolume())
        elif(instanceId == 'trackCtrl'):
            if direction == 'up':
                squeeze.nextTrack()
            else:
                squeeze.previousTrack()            
    
if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', filename='traces.log',filemode='w', level=logging.DEBUG)

    print("Starting...")

    # init mqtt
    ########################################
    broker_address="127.0.0.1"
    logging.info("Creating new MQTT client")
    client = mqtt.Client("MainApplication")
    logging.info("Connecting to MQTT broker")

    # todo - find a better solution for overall startup
    not_connected = True
    while not_connected:
        try:
            client.connect(broker_address)
            not_connected = False
        except socket.error:
            logging.warning('Cannot not connect to MQTT broker. Try again in 1 second')
            # Try again
            time.sleep(1)
    
    client.loop_start()

    # subscribe to events from the rotary controllers
    rotaryReceiver = IRotaryControllerReceiver()
    rotaryReceiver.registerForCallbacks(client, RotaryControllerCallback())

    # subscribe to events from the squeezeclient
    squeezeReceiver = ISqueezePlayerReceiver()
    squeezeReceiver.registerForCallbacks(client, SqueezePlayerCallback())
    
    # init presentation controller
    presCtrl = PresentationController()
    presCtrl.start()

    # init ability to publish messages to the display
    displayContent = IDisplayContentPublisher(client)
    displayContent.fireGeneralMessage('Waiting for', 'Connection')

    # connect to squeeze server and player
    squeeze = SqueezeliteManager()
    squeeze.start()

    # init volume controller
    volumeCtrl = UIController("volumeCtrl", CLOCKPIN_VLM_CTRL, DATAPIN_VLM_CTRL, SWITCHPIN_VLM_CTRL)
    volumeCtrl.start()

    # init track controller
    trackCtrl = UIController("trackCtrl", CLOCKPIN_TRK_CTRL, DATAPIN_TRK_CTRL, SWITCHPIN_TRK_CTRL)
    trackCtrl.start()

    # endless loop. polling squeezelite player for changes
    while True:
          try:
              squeeze.checkForChanges()
          except UnicodeEncodeError:
              logging.error ("Unknown Encoding")
          time.sleep(2)

          



